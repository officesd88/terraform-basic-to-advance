resource "google_compute_instance" "jenkins-master" {
    name = "jenkins-master"
    machine_type = "n1-standard-1"
    zone = "us-central1-c"
    allow_stopping_for_update = true
    #tags = ["MyTag_in_VPC"]

    labels = {
        name = "jenkins-master"
        owner = "jordan"
        user = "jordan"
        dept = "it"
        os = "ubuntu"
    }
    boot_disk {
      initialize_params{
        image = "ubuntu-os-cloud/ubuntu-1804-lts"
      }
    }
    network_interface{
        network = "default"

        access_config {
            // Ephemeral IP
        }
    }
    metadata_startup_script = "echo 'hi There'> /test.txt"

    service_account {
        email = "terraform-gcp-2022@fluent-cosine-355512.iam.gserviceaccount.com"
        scopes = ["cloud-platform"]

    }

    
  
}