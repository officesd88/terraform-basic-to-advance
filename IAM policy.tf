{
    "Version": "2012-10-17"
    "Statement": [
        {
            "Effect": "Allow"
            "Action": [
                "ec2:DeleteTags", # for only delete ec2 users
                "ec2:CreateTags"  # for only create ec2 users
            ],
            "Resources": "*" # * used for all access
        }
    ]
}
