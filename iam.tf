resource "aws_iam_user" "admin-user" {
    name = "sirshendu"
    tags = {
        Description : "Cloud Engineer II"
    }

resource "aws_iam_policy" "adminUser" {
    name = "AdminUsers"
    policy = file("admin-policy.json")
}
resource "aws_iam_policy_attachment" "sirshendu-admin-access" {
    user = aws_iam_user.admin-user.name
    policy_arn = aws_iam_policy_attachment.adminUser.arn

}
}
