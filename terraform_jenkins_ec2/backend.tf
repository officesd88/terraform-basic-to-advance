# --- root/backend.tf ---

terraform {
  backend "s3" {
    bucket = "terraform-jenkins-2022"
    key    = "remote.tfstate"
    region = "us-east-1"
  }
}